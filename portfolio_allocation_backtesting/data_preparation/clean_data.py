

class CleanData:
    def __init__(self, df):
        self.df = df

    def workflow(self):
        return self.df.ffill().bfill()