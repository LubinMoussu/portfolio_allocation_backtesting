import pandas as pd
from pypfopt import expected_returns
from pypfopt import risk_models
from pypfopt.efficient_frontier import EfficientFrontier

import matplotlib.pyplot as plt
import pypfopt


class DataEnricher:
    """
    Enrich with indicators
    """

    initial_investment = 10000

    def __init__(self, df):
        if not isinstance(df, pd.DataFrame):
            raise TypeError
        self.df = df

    def enrich(self):
        """

        :rtype: object
        """
        return self.df

