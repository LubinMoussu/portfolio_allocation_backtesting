from portfolio_allocation_backtesting.data_analysis.optimizer import Optimizer
from collections import OrderedDict

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
import pypfopt
import seaborn as sns
from pypfopt import HRPOpt
from pypfopt import black_litterman
from pypfopt import expected_returns
from pypfopt import risk_models
from pypfopt.cla import CLA
from pypfopt.discrete_allocation import DiscreteAllocation, get_latest_prices
from pypfopt.efficient_frontier import EfficientCVaR
from pypfopt.efficient_frontier import EfficientFrontier
from pypfopt.risk_models import CovarianceShrinkage

import numpy as np
import pandas as pd
import yfinance as yf
from datetime import datetime
import pytz


class EmaOptimizer(Optimizer):
    def __init__(self, df):
        super().__init__(df)

    def workflow(self):
        portfolio = self.df
        weight = self.df.shape[1]
        weight_dict = {k: 1 / weight for k in self.df.columns}
        weights = OrderedDict(weight_dict)
        allocation, leftover = self.get_discrete_allocation(weights, portfolio, method="EWA")
