from collections import OrderedDict

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
import pypfopt
import seaborn as sns
from pypfopt import HRPOpt
from pypfopt import black_litterman
from pypfopt import expected_returns
from pypfopt import risk_models
from pypfopt.cla import CLA
from pypfopt.discrete_allocation import DiscreteAllocation, get_latest_prices
from pypfopt.efficient_frontier import EfficientCVaR
from pypfopt.efficient_frontier import EfficientFrontier
from pypfopt.risk_models import CovarianceShrinkage

import numpy as np
import pandas as pd
import yfinance as yf
from datetime import datetime
import pytz

import matplotlib.pyplot as plt
import seaborn as sns

from pypfopt.efficient_frontier import EfficientFrontier
from pypfopt import risk_models
from pypfopt import expected_returns
from pypfopt import base_optimizer
from pypfopt import objective_functions
from pypfopt.discrete_allocation import DiscreteAllocation, get_latest_prices
from pypfopt import EfficientSemivariance
from pypfopt.expected_returns import mean_historical_return
from pypfopt.expected_returns import returns_from_prices



class Optimizer:
    """
    Enrich with indicators
    """

    initial_investment = 5000
    risk_free_rate = 0.02
    weight_bounds = (-1, 1)
    weight_bounds = (0, 1)
    # weight_bounds = (0.01, 0.1)

    expected_return = None
    volatility = None
    sharpe_ratio = 0
    allocation = {}
    leftover = None
    verbose = False

    def __init__(self, df):
        if not isinstance(df, pd.DataFrame):
            raise TypeError
        self.df = df

    def get_daily_returns(self):
        daily_returns = self.df.pct_change().dropna()
        fig = px.line(daily_returns, title='Daily Returns')
        fig.show()
        return daily_returns

    def get_daily_volatility(self, daily_returns):
        daily_returns.std()
        sns.displot(data=daily_returns, kind='kde', aspect=2.5)
        plt.xlim(-0.1, 0.1)

    def plot_cum_returns(self, data, title):
        daily_cum_returns = 1 + data.dropna().pct_change()
        daily_cum_returns = daily_cum_returns.cumprod() * 100
        return px.line(daily_cum_returns, title=title)

    def individual_cumulative_returns(self):
        fig_cum_returns = self.plot_cum_returns(self.df, 'Cumulative Returns of Individual Stocks Starting with $100')
        fig_cum_returns.show()

    def get_correlation_matrix(self):
        corr_df = self.df.corr().round(2)  # round to 2 decimal places
        fig_corr = px.imshow(corr_df, text_auto=True, title='Correlation between Stocks')
        fig_corr.show()

    def stats(self):
        daily_returns = self.get_daily_returns()
        self.get_daily_volatility(daily_returns)
        self.individual_cumulative_returns()
        self.get_correlation_matrix()

    def test(self):
        """

        :rtype: object
        """
        # Annualized Return
        mu = expected_returns.mean_historical_return(self.df)
        # Sample Variance of Portfolio

        sigma = risk_models.sample_cov(self.df)
        # Max Sharpe Ratio - Tangent to the EF
        ef = EfficientFrontier(mu, sigma,
                               weight_bounds=self.weight_bounds)

        sharpe_pfolio = ef.max_sharpe(
            risk_free_rate=self.risk_free_rate)  # May use add objective to ensure minimum zero weighting to individual stocks
        sharpe_pwt = ef.clean_weights()

        sharpe_pfolio = pd.DataFrame.from_dict(sharpe_pfolio, orient='index')
        weights_df = pd.DataFrame.from_dict(sharpe_pwt, orient='index')
        print("These are the raw weights: ", sharpe_pfolio)
        print("These are the cleaned weights: ", weights_df)
        ef.portfolio_performance(verbose=self.verbose)

        latest_prices = pypfopt.discrete_allocation.get_latest_prices(self.df)
        da = pypfopt.discrete_allocation.DiscreteAllocation(sharpe_pwt, latest_prices,
                                                            total_portfolio_value=self.initial_investment)
        allocation, leftover = da.greedy_portfolio()
        # da.lp_portfolio()
        #
        # print(allocation)
        print("Discrete allocation:", allocation)
        print("Funds remaining: Rs.{:.2f}".format(leftover))

        cla = CLA(mu, sigma)
        print(cla)

    def get_discrete_allocation(self, weights, portfolio, method: str):
        latest_prices = get_latest_prices(portfolio)
        da = DiscreteAllocation(weights, latest_prices, total_portfolio_value=self.initial_investment)
        allocation, leftover = da.greedy_portfolio()
        print(f"Discrete allocation ({method}):", allocation)
        print(f"Funds remaining({method}): ${leftover}")
        allocation, leftover = da.lp_portfolio()
        print(f"Discrete allocation({method}):", allocation)
        print(f"Funds remaining({method}): ${leftover}")
        print()
        return allocation, leftover

    def set_results(self, expected_return, volatility, sharpe_ratio, allocation, leftover):
        self.expected_return = expected_return
        self.volatility = volatility
        self.sharpe_ratio = sharpe_ratio
        self.allocation = allocation
        self.leftover = leftover

    def get_mean_variance_optimization_ema(self):
        portfolio = self.df
        mu = expected_returns.ema_historical_return(portfolio)
        S = risk_models.exp_cov(portfolio)
        ef = EfficientFrontier(mu, S, weight_bounds=self.weight_bounds)
        weights = ef.max_sharpe(
            risk_free_rate=self.risk_free_rate)

        expected_return, volatility, sharpe_ratio = ef.portfolio_performance(verbose=self.verbose)
        allocation, leftover = self.get_discrete_allocation(weights, portfolio, method="ema")
        self.set_results(expected_return, volatility, sharpe_ratio, allocation, leftover)

    def test_(self):
        portfolio = self.df
        mu = expected_returns.capm_return(portfolio)
        S = risk_models.exp_cov(portfolio)
        ef = EfficientFrontier(mu, S, weight_bounds=self.weight_bounds)
        weights = ef.max_sharpe(
            risk_free_rate=self.risk_free_rate)

        expected_return, volatility, sharpe_ratio = ef.portfolio_performance(verbose=self.verbose)
        allocation, leftover = self.get_discrete_allocation(weights, portfolio, method="test")
        self.set_results(expected_return, volatility, sharpe_ratio, allocation, leftover)


    def get_mean_variance_optimization_constraints(self):
        portfolio = self.df
        mu = expected_returns.mean_historical_return(portfolio)
        S = CovarianceShrinkage(portfolio).ledoit_wolf()
        ef = EfficientFrontier(mu, S, weight_bounds=self.weight_bounds)
        weights = ef.max_sharpe(
            risk_free_rate=self.risk_free_rate)

        min_weight, max_weight = 0.05, 0.35
        constraints=[
            # {"type": "eq", "fun": lambda w: np.sum(w) - 1},  # sum to 1
            {"type": "ineq", "fun": lambda w: w - min_weight},  # greater than min_weight
            {"type": "ineq", "fun": lambda w: max_weight - w},  # less than max_weight
        ]

        weights = ef.nonconvex_objective(
            objective_functions.sharpe_ratio,
            objective_args=(mu, S),
            weights_sum_to_one=True,
            constraints = constraints
        )

        clean_weights = ef.clean_weights()
        symbols = list(self.df.columns)
        weights = np.array([clean_weights[symbol] for symbol in symbols])
        best_sharpe_ratio = base_optimizer.portfolio_performance(weights, mu, S)[2]
        print(best_sharpe_ratio)

        allocation, leftover = self.get_discrete_allocation(weights, portfolio, method="MVO")