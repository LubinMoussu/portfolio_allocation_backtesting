from portfolio_allocation_backtesting.data_analysis.optimizer import Optimizer
from collections import OrderedDict

import matplotlib.pyplot as plt
import pandas as pd
import plotly.express as px
import pypfopt
import seaborn as sns
from pypfopt import HRPOpt
from pypfopt import black_litterman
from pypfopt import expected_returns
from pypfopt import risk_models
from pypfopt.cla import CLA
from pypfopt.discrete_allocation import DiscreteAllocation, get_latest_prices
from pypfopt.efficient_frontier import EfficientCVaR
from pypfopt.efficient_frontier import EfficientFrontier
from pypfopt.risk_models import CovarianceShrinkage

import numpy as np
import pandas as pd
import yfinance as yf
from datetime import datetime
import pytz


class HrpOptimizer(Optimizer):
    def __init__(self, df):
        super().__init__(df)

    def workflow(self):
        portfolio = self.df
        returns = portfolio.pct_change().dropna()
        hrp = HRPOpt(returns)
        hrp_weights = hrp.optimize()
        expected_return, volatility, sharpe_ratio = hrp.portfolio_performance(verbose=self.verbose)
        allocation, leftover = self.get_discrete_allocation(hrp_weights, portfolio, method="HRP")
        self.set_results(expected_return, volatility, sharpe_ratio, allocation, leftover)
