from portfolio_allocation_backtesting.data_acquisition.import_data import ImportData
from portfolio_allocation_backtesting.data_analysis import mcvar_analyser, ema_analyser, hrp_analyser, mvo_analyser
from portfolio_allocation_backtesting.data_analysis.optimizer import Optimizer
from portfolio_allocation_backtesting.data_preparation.build_features import DataEnricher
from portfolio_allocation_backtesting.data_preparation.clean_data import CleanData

_ = [mcvar_analyser.McvarOptimizer, ema_analyser.EmaOptimizer, hrp_analyser.HrpOptimizer, mvo_analyser.MvoOptimizer]


def all_subclasses(cls):
    return set(cls.__subclasses__()).union(
        [s for c in cls.__subclasses__() for s in all_subclasses(c)])


class Handler:
    def __init__(self, stock_tickers):
        self.stock_tickers = stock_tickers

    def acquire_data(self):
        maker = ImportData(ticker_list=self.stock_tickers)
        return maker.workflow()

    @staticmethod
    def prepare_data(df_stocks):
        # explore data
        # visulize
        # clean data
        data_cleaner = CleanData(df=df_stocks)
        cleaned_data = data_cleaner.workflow()
        enricher = DataEnricher(df=cleaned_data)
        return enricher.enrich()

    def implement_method(self, df):
        analyser = Optimizer(df=df)
        # analyser.workflow()
        # return analyser

    @staticmethod
    def sort_optimizer_by_sharp(enriched_instance_list):
        """

        :param enriched_instance_list:
        :return:
        """
        enriched_instance_list.sort(key=lambda x: x.sharpe_ratio)
        return enriched_instance_list

    def call_methods(self, df):
        all_subclasses_to_iter = all_subclasses(Optimizer)
        instance_list = []
        for subclass in all_subclasses_to_iter:
            instance = subclass(df)
            instance.workflow()
            instance_list.append(instance)
        return self.sort_optimizer_by_sharp(instance_list)

    def find_optimizer_maximizing_sharpe_ratio(self, df):
        all = self.call_methods(df)
        for elt in all:

            print(elt.sharpe_ratio, len(elt.allocation)/len(stock_tickers), elt.allocation)
        best = all[-1]
        allocations = best.allocation
        print(best.sharpe_ratio, best.allocation)
        print(len(allocations)/len(stock_tickers))

    # def find_good_constraints(self):


    def analyse_data(self, df):
        # feature selection
        # model selection
        # analyse result
        self.find_optimizer_maximizing_sharpe_ratio(df)


    def workflow(self):
        df_stocks = self.acquire_data()
        prepared_df_stocks = self.prepare_data(df_stocks)
        self.analyse_data(prepared_df_stocks)


if __name__ == "__main__":
    # stock_tickers = ['AI.PA', 'AC.PA', 'SU.PA', 'MC.PA', 'OR.PA', 'RI.PA', 'CAP.PA', 'SW.PA', 'FP.VI', 'BN.PA']
    # stock_tickers = ['AI.PA', 'AC.PA', 'SU.PA', 'MC.PA', 'OR.PA', 'RI.PA', 'CAP.PA', 'SW.PA', 'FP.VI', 'BN.PA',
    #                  'RMS.PA', 'ALESE.PA', 'NEOEN.PA', "TTE.PA", 'CS.PA', "SAN.PA"]
    stock_tickers = ['AI.PA', 'AC.PA', 'SU.PA']
    handler = Handler(stock_tickers=stock_tickers).workflow()
