import matplotlib.pyplot as plt
import pypfopt
from pandas import DataFrame

plt.style.use('ggplot')
import plotly.express as px


class IndicatorPlot:
    def __init__(self, df):
        if not isinstance(df, DataFrame):
            raise TypeError
        self.df = df

    def plot_individual_stock_prices(self):
        fig_price = px.line(self.df, title='Price of Individual Stocks')
        fig_price.show()

    def plot(self):
        latest_prices = pypfopt.discrete_allocation.get_latest_prices(self.df)

        minvol_pwt = {column: 0.55 for column in self.df.columns.tolist()}

        allocation_minv, rem_minv = pypfopt.discrete_allocation.DiscreteAllocation(minvol_pwt, latest_prices,
                                                                                   total_portfolio_value=10000).lp_portfolio()
        print(allocation_minv)
        print("Leftover Fund value in$ after building minimum volatility portfolio is ${:.2f}".format(rem_minv))
