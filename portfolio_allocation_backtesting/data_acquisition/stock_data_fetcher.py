import numpy as np
import pandas as pd
import yfinance as yf


class StockDataFetcher:
    start_date = '2019-01-01'
    end_date = '2021-06-12'
    time_interval = 'daily'
    period = "2y"
    decimals = 3
    interval = "1d"

    def __init__(self, ticker=None):
        if ticker is None:
            ticker = 'BTC-USD'
        self.ticker = ticker
        self.company_name = ""

    def fetch_stock_prices(self):
        """

        :return:
        """
        msft = yf.Ticker(self.ticker)
        try:
            self.company_name = msft.info['longName']
        except KeyError:
            pass
        basedata = msft.history(period=self.period, interval=self.interval)
        basedata = basedata.reset_index()
        basedata.set_index(pd.DatetimeIndex(basedata["Date"]), inplace=True)
        return basedata

    def fetch_yahoo_tickers(self):
        """

        :return:
        """
        return yf.download(tickers=self.ticker,
                           start=self.start_date,
                           auto_adjust=True
                           )

    def clean_data(self, df_to_clean):
        # drop rows containing nan, -inf, inf values
        df_to_clean.replace([np.inf, -np.inf], np.nan, inplace=True)
        df_to_clean.dropna(inplace=True)

        # rename columns to lower case
        df_to_clean.columns = df_to_clean.columns.str.lower()

        # get columns that have float64 datatype
        data_to_type = dict(df_to_clean.dtypes)
        column_with_float64 = [k for k, v in data_to_type.items() if v == 'float64']

        # round all 'float64'
        df_to_clean[column_with_float64].round(decimals=self.decimals)
        return df_to_clean

    def fetch_cleaned_stock_prices(self):
        """

        :return:
        """
        fetched_df = self.fetch_stock_prices()
        fetched_df["ticker"] = self.ticker
        return self.clean_data(fetched_df)


if __name__ == "__main__":
    stock_tickers = ['AI.PA', 'AC.PA', 'SU.PA', 'MC.PA', 'OR.PA', 'RI.PA', 'CAP.PA', 'SW.PA', 'FP.VI', 'BN.PA']
    cryptos = ['ETH-USD']

    fetcher = StockDataFetcher(ticker='ETH-USD')
    df = fetcher.fetch_cleaned_stock_prices()
    print(fetcher.company_name)
