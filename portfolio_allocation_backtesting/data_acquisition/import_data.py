import pandas

from portfolio_allocation_backtesting.data_acquisition.stock_data_fetcher import StockDataFetcher


class ImportData:
    def __init__(self, ticker_list, stock_price_fetcher=StockDataFetcher()):
        self.df = None
        self.ticker_list = ticker_list
        self.stock_price_fetcher = stock_price_fetcher

    def scrap_multiple_tickers(self):
        price_data = []
        for ticker in self.ticker_list:
            self.stock_price_fetcher.ticker = ticker
            prices = self.stock_price_fetcher.fetch_cleaned_stock_prices()
            prices = prices[["open"]]
            prices.rename(columns={'open': ticker}, inplace=True)
            price_data.append(prices)

        return pandas.concat(price_data, axis=1)

    def workflow(self):
        """

        :return:
        """
        return self.scrap_multiple_tickers()


if __name__ == "__main__":
    ticker_list = ['AI.PA', 'AC.PA', 'SU.PA']
    maker = ImportData(ticker_list=ticker_list)
    df = maker.scrap_multiple_tickers()
    print(df)
